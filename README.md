# Presenting Projects to the World

Before starting with best practices let's understand the mindset of a marketer. 

## Marketing Mindset and Best Practices

> **Marketing is your ability to present a product in a way that excites the customer (within the 2 second attention span that every one today has).**
This project will give you an understanding of mindset needed to market a project and best practices for it. 

## Purpose : 
Get aligned to the mindset of a marketer and best practices for it.

## Problem this module solves for you

How much time do you spend time reading, re-reading or admiring each word of content you are reading? Or how much time do you spend seeing a graphical content admiring its beauty?

You know the answer !! But naive content creators/marketers make the same mistake. Spend too much time perfecting their content forgetting the audience behaviour.

For your audience content is available in abundance so no matter how perfect your content is, your TG is not going to spend a lot of time on it.

Instead, if you create a lot of optimised content(key eye positions have relevant stuff) consistently, it has greater possibilty to drive your TG towards taking action. This module will bridge this mindset gap for you and also skill you on using latest tools and best practices to create content fast.


## Goal :

## What will you learn in this module? 
1. Mindset of a Marketer(Attract Attention)
1. Effective Communication
1. Blog Writing Principles

## How to get started with it?

#### Start with the Understand Stage by going to [wiki](https://gitlab.com/iotiotdotin/community-projects/presenting-projects-to-the-world/-/wikis/home) and then implement learnings for your project.


